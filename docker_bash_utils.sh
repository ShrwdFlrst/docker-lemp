#!/usr/bin/env bash
function docker_utils_start() {
    docker-compose up -d --force-recreate --build
}

function docker_utils_get_ip() {
    docker-machine ip default
}

function docker_utils_get_running() {
    docker-machine ls
}

function docker_utils_list() {
    # Alternative to 'docker-compose ps -q'
    docker ps -a | grep $1 | awk '{print $1}'
}

function docker_utils_destroy() {
    echo "Destroying container $1"
    docker rm -vf "$1"
}

function docker_utils_destroy_all() {
    for i in `docker_utils_list $1`; do
        docker_utils_destroy $i;
    done
}

function docker_utils_logs() {
    docker-compose logs -f
}

function docker_utils_vb_on() {
    docker-machine active || docker-machine create --driver virtualbox default
    docker-machine start default
    eval $(docker-machine env default)
}

function docker_utils_vb_off() {
    docker-machine stop default
    unset ${!DOCKER_*}
}

# docker-machine stop
# docker-machine start
# docker inspect dockertutorial_php_1
# docker exec -i -t dockertutorial_php_1 /bin/bash
# while true; do rsync --exclude='.git/' --exclude='.idea/' --exclude='app/cache/' --exclude='node_modules' -avzq --stats /src/ /var/www/mbaco/; sleep 0.4; done
